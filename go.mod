module gitlab.com/project_falcon/kubeless/klib/ingresslego

go 1.14

require (
	gitlab.com/project_falcon/kubeless/klib/job v0.4.0
	gitlab.com/project_falcon/kubeless/klib/toolslego v0.24.6
	gitlab.com/project_falcon/kubeless/lib/payload v1.50.3
	k8s.io/api v0.19.4
	k8s.io/apimachinery v0.19.4
)
