package ingresslego

import (
	"fmt"
	"strings"

	"gitlab.com/project_falcon/kubeless/klib/job"
	"gitlab.com/project_falcon/kubeless/klib/toolslego"
	"gitlab.com/project_falcon/kubeless/lib/payload"
	v1Beta1 "k8s.io/api/extensions/v1beta1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
)

const (
	service      = "ingresslego"
	cutPublicURL = 47
)

func getIngressPath(serviceName string, ports map[string]int32) []v1Beta1.HTTPIngressPath {
	var ingressPath []v1Beta1.HTTPIngressPath

	for _, port := range ports {
		path := v1Beta1.HTTPIngressPath{
			Path: "/",
			Backend: v1Beta1.IngressBackend{
				ServiceName: serviceName,
				ServicePort: intstr.FromInt(int(port)),
			},
		}

		ingressPath = append(ingressPath, path)
	}

	return ingressPath
}

func getIngressRules(fullURL string, serviceName string, jobDef *job.BuildConfig) []v1Beta1.IngressRule {
	var rules []v1Beta1.IngressRule

	for _, container := range jobDef.Run {
		if len(container.Ports) > 0 {
			ing := v1Beta1.IngressRule{
				Host: fullURL,
				IngressRuleValue: v1Beta1.IngressRuleValue{
					HTTP: &v1Beta1.HTTPIngressRuleValue{
						Paths: getIngressPath(serviceName, container.Ports),
					},
				},
			}

			rules = append(rules, ing)
		}
	}

	return rules
}

func GetIngressDef(box *payload.Box, kconfig *toolslego.KConfig, jobDef *job.BuildConfig, IDTask string) (*v1Beta1.Ingress,string) {

	serviceName := toolslego.GetBaseName(payload.DK8sCreateDeploy, box.Project)
	fullURL := getPreffixURL(box, kconfig, IDTask)

	ing := v1Beta1.Ingress{
		ObjectMeta: metaV1.ObjectMeta{
			Name:        serviceName,
			Labels:      toolslego.GetBoxLabels(*box, serviceName),
			Annotations: getAnnotations(kconfig, box),
		},

		Spec: v1Beta1.IngressSpec{
			Rules: getIngressRules(fullURL, serviceName, jobDef),
		},
	}

	return &ing, fullURL
}

func getPreffixURL(box *payload.Box, kconfig *toolslego.KConfig, IDTask string) string {
	partURL := fmt.Sprintf("%v-%v-%v", box.Project, box.Brand, box.Branch.Name)

	runes := []rune(partURL)

	if len(runes) > cutPublicURL {
		partURL = string(runes[1:cutPublicURL])
	}

	fullURL := fmt.Sprintf("%v-%v.%v", partURL, IDTask, kconfig.SuffixDomain)

	return strings.ToLower(fullURL)
}

func getAnnotations(kconfig *toolslego.KConfig, box *payload.Box) map[string]string {

	title := fmt.Sprintf("Authentication Required: %v",box.Project)

	ano := make(map[string]string)
	ano["nginx.ingress.kubernetes.io/auth-type"] = "basic"
	ano["nginx.ingress.kubernetes.io/auth-realm"] = title
	ano["nginx.ingress.kubernetes.io/auth-secret"] = fmt.Sprintf("%v/%v-auth", kconfig.Namespace, box.Project)

	return ano
}
